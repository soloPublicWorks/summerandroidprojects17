package ru.geekbrains.android.level1.calculatorsimple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class CalculatorActivity extends AppCompatActivity {
    public static final int DIGITS_NUMBER = 10;
    public static final int OPERATIONS_NUMBER = 4;
    public static final int NUMBERS_NUMBER = 2;
    private static final String KEY_NUMBER1 = "number1";
    private static final String KEY_NUMBER2 = "number2";
    private static final String KEY_OPERATION = "operation";
    private static final String KEY_RESULT = "result";

    private EditText[] etNumbers = new EditText[NUMBERS_NUMBER];
    private int focusedEditTextNumber;
    private EditText etOperation;
    private EditText etResult;
    private Button[] btnDigits = new Button[DIGITS_NUMBER];
    private Button[] btnOperations = new Button[OPERATIONS_NUMBER]; // +, -, *, /
    private Button btnClear, btnCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        initViews();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        etNumbers[0].setText(savedInstanceState.getString(KEY_NUMBER1));
        etNumbers[1].setText(savedInstanceState.getString(KEY_NUMBER2));
        etOperation.setText(savedInstanceState.getString(KEY_OPERATION));
        etResult.setText(savedInstanceState.getString(KEY_RESULT));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_NUMBER1, etNumbers[0].getText().toString());
        outState.putString(KEY_NUMBER2, etNumbers[1].getText().toString());
        outState.putString(KEY_OPERATION, etOperation.getText().toString());
        outState.putString(KEY_RESULT, etResult.getText().toString());

    }

    private void initViews() {
        // поля ввода
        initFields();
        // цифры
        initDigits();
        // операции
        initOperations();
        // управляющие кнопки
        initManagers();
    }
    private void initFields() {
        etNumbers[0] = (EditText) findViewById(R.id.et_first);
        etNumbers[0].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                focusedEditTextNumber = 0;
                return false;
            }
        });
        etNumbers[1] = (EditText) findViewById(R.id.et_second);
        etNumbers[1].setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                focusedEditTextNumber = 1;
                return false;
            }
        });
        etOperation = (EditText) findViewById(R.id.et_operation);
        etResult = (EditText) findViewById(R.id.et_result);
    }
    private void initDigits() {
        btnDigits[0] = (Button) findViewById(R.id.btn_0);
        btnDigits[1] = (Button) findViewById(R.id.btn_1);
        btnDigits[2] = (Button) findViewById(R.id.btn_2);
        btnDigits[3] = (Button) findViewById(R.id.btn_3);
        btnDigits[4] = (Button) findViewById(R.id.btn_4);
        btnDigits[5] = (Button) findViewById(R.id.btn_5);
        btnDigits[6] = (Button) findViewById(R.id.btn_6);
        btnDigits[7] = (Button) findViewById(R.id.btn_7);
        btnDigits[8] = (Button) findViewById(R.id.btn_8);
        btnDigits[9] = (Button) findViewById(R.id.btn_9);
        for (int i = 0; i < DIGITS_NUMBER; i++)
            btnDigits[i].setOnClickListener(new OnClickDigit((byte) i, etNumbers, this));
    }
    private void initOperations() {
        btnOperations[0] = (Button) findViewById(R.id.btn_add);
        btnOperations[0].setOnClickListener(new OnClickOperation('+', etOperation));
        btnOperations[1] = (Button) findViewById(R.id.btn_sub);
        btnOperations[1].setOnClickListener(new OnClickOperation('-', etOperation));
        btnOperations[2] = (Button) findViewById(R.id.btn_mult);
        btnOperations[2].setOnClickListener(new OnClickOperation('*', etOperation));
        btnOperations[3] = (Button) findViewById(R.id.btn_div);
        btnOperations[3].setOnClickListener(new OnClickOperation('/', etOperation));
    }
    private void initManagers() {
        btnClear = (Button) findViewById(R.id.btn_clear);
        btnClear.setOnClickListener(new OnClickManageButton(etNumbers, etOperation, etResult));
        btnCalculate = (Button) findViewById(R.id.btn_calculate);
        btnCalculate.setOnClickListener(new OnClickManageButton(etNumbers, etOperation, etResult));
    }

    public int getFocusedNumber() { return focusedEditTextNumber; }
}
