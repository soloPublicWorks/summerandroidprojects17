package ru.geekbrains.android.level1.calculatorsimple;

import android.view.View;
import android.widget.EditText;

/**
 * Created by Валера on 27.08.2017.
 */

public class OnClickOperation implements View.OnClickListener {
    private char operation;
    private EditText etOperation;

    public OnClickOperation(char operation, EditText etOperation) {
        this.operation = operation;
        this.etOperation = etOperation;
    }

    @Override
    public void onClick(View view) {
        etOperation.setText(String.valueOf(operation));
    }
}
