package ru.geekbrains.android.level1.calculatorsimple;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Валера on 27.08.2017.
 */

public class OnClickManageButton implements View.OnClickListener {
    private EditText[] etNumbers;
    private EditText etOperation;
    private EditText etResult;

    public OnClickManageButton(EditText[] etNumbers, EditText etOperation, EditText etResult) {
        this.etNumbers = etNumbers;
        this.etOperation = etOperation;
        this.etResult = etResult;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_clear:
                clearFields();
                break;
            case R.id.btn_calculate:
                calculate();
                break;
        }
    }

    private void clearFields() {
        for (EditText etNumber: etNumbers) etNumber.setText("");
        etOperation.setText("");
        etResult.setText("");
    }

    private void calculate() {
        if (fieldsFilled()) {
            // гарантируется преобразование без исключения
            int number1 = Integer.valueOf(etNumbers[0].getText().toString());
            int number2 = Integer.valueOf(etNumbers[1].getText().toString());
            String result = "";
            switch (etOperation.getText().toString()) {
                case "+":
                    result = String.valueOf(number1 + number2);
                    break;
                case "-":
                    result = String.valueOf(number1 - number2);
                    break;
                case "*":
                    result = String.valueOf(number1 * number2);
                    break;
                case "/":
                    result = String.valueOf(number1 / ((double) number2));
                    break;
                default:
                    Toast.makeText(null, R.string.error_illegal_operation, Toast.LENGTH_SHORT).show();
            }
            etResult.setText(result);
        } else {
            Toast.makeText(null, R.string.error_input, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean fieldsFilled() {
        boolean fieldsFilled = true;
        for (int i = 0; i < etNumbers.length && fieldsFilled; i++)
            if (etNumbers[i].getText().toString().equals("")) fieldsFilled = false;
        fieldsFilled &= !etOperation.getText().toString().equals("");
        return fieldsFilled;
    }
}
