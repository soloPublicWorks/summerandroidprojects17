package ru.geekbrains.android.level1.calculatorsimple;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Валера on 27.08.2017.
 */

public class OnClickDigit implements View.OnClickListener {
    private byte digit;
    private EditText[] etNumbers;
    private CalculatorActivity activity;

    public OnClickDigit(byte digit, EditText[] etNumbers, CalculatorActivity activity) {
        this.digit = digit;
        this.etNumbers = etNumbers;
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        switch (activity.getFocusedNumber()) {
            case 0:
                etNumbers[0].setText(etNumbers[0].getText().toString() + digit);
                break;
            case 1:
                etNumbers[1].setText(etNumbers[1].getText().toString() + digit);
                break;
        }
    }
}
