package ru.geekbrains.android.level1.starwarshandbook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class HandbookActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView ivCharacter;
    private Spinner spnrCharactersList;
    private TextView tvDescription;
    private CharacterList characterList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handbook);
        characterList = new CharacterList(this);
        initViews();
    }

    private void initViews() {
        ivCharacter = (ImageView) findViewById(R.id.iv_character);
        tvDescription = (TextView) findViewById(R.id.tv_character_description);

        spnrCharactersList = (Spinner) findViewById(R.id.spnr_characters_list);
        spnrCharactersList.setAdapter(new HandbookSpinnerAdapter(characterList));
        spnrCharactersList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                onClick(null);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        onClick(null);
    }

    @Override
    public void onClick(View view) {
        ivCharacter.setImageDrawable(characterList.getImage(spnrCharactersList.getSelectedItem().toString()));
        tvDescription.setText(characterList.getDescription(spnrCharactersList.getSelectedItem().toString()));
    }
}
