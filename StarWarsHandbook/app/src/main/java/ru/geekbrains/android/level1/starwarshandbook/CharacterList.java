package ru.geekbrains.android.level1.starwarshandbook;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * Created by Валера on 22.08.2017.
 */

public class CharacterList {
    private Context context;
    private String[] characters;

    public CharacterList(Context context) {
        this.context = context;
        characters = context.getResources().getStringArray(R.array.characters);
    }

    public String getDescription(String name) {
        if (name.equals(characters[0])) {
            return context.getResources().getString(R.string.description_C3PO);
        } else if (name.equals(characters[1])) {
            return context.getResources().getString(R.string.description_BB8);
        } else if (name.equals(characters[2])) {
            return context.getResources().getString(R.string.description_R2D2);
        } else if (name.equals(characters[3])) {
            return context.getResources().getString(R.string.description_dart_weider);
        } else if (name.equals(characters[4])) {
            return context.getResources().getString(R.string.description_сlone_trooper);
        } else if (name.equals(characters[5])) {
            return context.getResources().getString(R.string.description_boba_fett);
        } else return context.getResources().getString(R.string.description_unknown);
    }
    public Drawable getIcon(String name){
        if(name.equals(characters[0])) return context.getResources().getDrawable(R.mipmap.ic_c3po);
        if(name.equals(characters[1])) return context.getResources().getDrawable(R.mipmap.ic_bb8);
        if(name.equals(characters[2])) return context.getResources().getDrawable(R.mipmap.ic_r2d2);
        if(name.equals(characters[3])) return context.getResources().getDrawable(R.mipmap.ic_dart_weider);
        if(name.equals(characters[4])) return context.getResources().getDrawable(R.mipmap.ic_clone_trooper);
        if(name.equals(characters[5])) return context.getResources().getDrawable(R.mipmap.ic_boba_fett);
        else return null;
    }
    public Drawable getImage(String name){
        if(name.equals(characters[0])) return context.getResources().getDrawable(R.drawable.c3po);
        if(name.equals(characters[1])) return context.getResources().getDrawable(R.drawable.bb8);
        if(name.equals(characters[2])) return context.getResources().getDrawable(R.drawable.r2d2);
        if(name.equals(characters[3])) return context.getResources().getDrawable(R.drawable.dart_weider);
        if(name.equals(characters[4])) return context.getResources().getDrawable(R.drawable.clone_trooper);
        if(name.equals(characters[5])) return context.getResources().getDrawable(R.drawable.boba_fett);
        else return null;
    }
    public String[] getCharacters() {
        return characters;
    }
    public int getSize() {
        return characters.length;
    }
}
