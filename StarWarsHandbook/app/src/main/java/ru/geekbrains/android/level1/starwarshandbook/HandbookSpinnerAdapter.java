package ru.geekbrains.android.level1.starwarshandbook;

import android.database.DataSetObserver;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.Icon;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

/**
 * Created by Валера on 22.08.2017.
 */

public class HandbookSpinnerAdapter implements SpinnerAdapter {
    private CharacterList characterList;

    public HandbookSpinnerAdapter(CharacterList characterList) {
        this.characterList = characterList;
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spinner_element, null);
        }
        TextView tvName = view.findViewById(R.id.tv_character_name);
        tvName.setText(characterList.getCharacters()[i]);
        ImageView ivIcon = view.findViewById(R.id.iv_icon_character);
        ivIcon.setImageDrawable(characterList.getIcon(tvName.getText().toString()));
        return view;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.spinner_element_selected, null);
        }
        TextView tvName = view.findViewById(R.id.tv_character_name_selected);
        tvName.setText(characterList.getCharacters()[i]);
        ImageView ivIcon = view.findViewById(R.id.iv_icon_character_selected);
        ivIcon.setImageDrawable(characterList.getIcon(tvName.getText().toString()));
        return view;
    }

    @Override
    public Object getItem(int i) {
        return characterList.getCharacters()[i];
    }
    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }
    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }
    @Override
    public int getCount() {
        return characterList.getSize();
    }
    @Override
    public long getItemId(int i) {
        return 1;
    }
    @Override
    public boolean hasStableIds() {
        return false;
    }
    @Override
    public int getItemViewType(int i) {
        return 1;
    }
    @Override
    public int getViewTypeCount() {
        return 1;
    }
    @Override
    public boolean isEmpty() {
        return false;
    }
}
