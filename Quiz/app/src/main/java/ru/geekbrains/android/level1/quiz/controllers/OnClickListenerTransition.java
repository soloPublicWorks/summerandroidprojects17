package ru.geekbrains.android.level1.quiz.controllers;

import android.view.View;
import android.widget.Toast;

import ru.geekbrains.android.level1.quiz.QuizActivity;
import ru.geekbrains.android.level1.quiz.models.Quiz;

/**
 * Created by Валера on 21.08.2017.
 */

public abstract class OnClickListenerTransition implements View.OnClickListener {
    protected Quiz quiz;
    protected QuizActivity quizActivity;

    public OnClickListenerTransition(Quiz quiz, QuizActivity quizActivity) {
        this.quiz = quiz;
        this.quizActivity = quizActivity;
    }

    @Override
    public void onClick(View view) {
        if (quiz.isPassed()) quizActivity.showResult();
        quizActivity.resetPreviousAnswer(false);
        quizActivity.setQuestion(quiz.getCurrentQuestion());
    }
}
