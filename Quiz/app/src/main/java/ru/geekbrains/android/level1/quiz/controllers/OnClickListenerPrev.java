package ru.geekbrains.android.level1.quiz.controllers;

import android.view.View;
import android.widget.Toast;

import ru.geekbrains.android.level1.quiz.QuizActivity;
import ru.geekbrains.android.level1.quiz.models.Quiz;

/**
 * Created by Валера on 21.08.2017.
 */

public class OnClickListenerPrev extends OnClickListenerTransition {
    public OnClickListenerPrev(Quiz quiz, QuizActivity quizActivity) {
        super(quiz, quizActivity);
    }

    @Override
    public void onClick(View view) {
        quiz.getPreviousQuestion();
        super.onClick(view);
    }
}
