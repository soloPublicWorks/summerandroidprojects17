package ru.geekbrains.android.level1.quiz.models;

/**
 * Created by Валера on 21.08.2017.
 */

public class Question {
    private String text;
    private Boolean answer;
    private boolean isAnswered;

    public Question(String text, Boolean answer) {
        this.text = text;
        this.answer = answer;
    }

    public String getText() {
        return text;
    }
    public Boolean getAnswer() {
        return answer;
    }
    public boolean isAnswered() {
        return isAnswered;
    }

    public void setIsAnswered() {
        isAnswered = true;
    }
}
