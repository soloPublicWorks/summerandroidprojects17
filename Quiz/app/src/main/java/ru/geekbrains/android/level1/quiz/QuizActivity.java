package ru.geekbrains.android.level1.quiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import ru.geekbrains.android.level1.quiz.controllers.OnClickListenerAnswer;
import ru.geekbrains.android.level1.quiz.controllers.OnClickListenerNext;
import ru.geekbrains.android.level1.quiz.controllers.OnClickListenerPrev;
import ru.geekbrains.android.level1.quiz.models.Question;
import ru.geekbrains.android.level1.quiz.models.Quiz;

public class QuizActivity extends AppCompatActivity {
    private Quiz quiz;

    private ImageButton btnNext, btnPrev;
    private Button btnTrue, btnFalse;
    private TextView tvQuestion;
    private Toast toastQuizResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        quiz = new Quiz(getAssets());
        initViews();
    }

    private void initViews() {
        btnNext = (ImageButton) findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new OnClickListenerNext(quiz, this));
        btnPrev = (ImageButton) findViewById(R.id.btn_prev);
        btnPrev.setOnClickListener(new OnClickListenerPrev(quiz, this));

        btnTrue = (Button) findViewById(R.id.btn_true);
        btnFalse = (Button) findViewById(R.id.btn_false);
        btnTrue.setOnClickListener(new OnClickListenerAnswer(true, btnTrue, quiz, this));
        btnFalse.setOnClickListener(new OnClickListenerAnswer(false, btnFalse, quiz, this));

        tvQuestion = (TextView) findViewById(R.id.tv_question);
        setQuestion(quiz.getCurrentQuestion());

        toastQuizResult = Toast.makeText(this, "", Toast.LENGTH_LONG);
    }

    public void resetPreviousAnswer(boolean shouldCancelToast) {
        btnTrue.setBackgroundColor(getResources().getColor(R.color.colorButton));
        btnFalse.setBackgroundColor(getResources().getColor(R.color.colorButton));
        if (shouldCancelToast) toastQuizResult.cancel();
    }
    public void setAnswer(boolean answer, Button btnAnswer) {
        resetPreviousAnswer(false);
        if (answer) {
            btnAnswer.setBackgroundColor(getResources().getColor(R.color.colorAnswerTrue));
        } else {
            btnAnswer.setBackgroundColor(getResources().getColor(R.color.colorAnswerFalse));
        }
    }
    public void setQuestion(Question question) {
        tvQuestion.setText(question.getText());
    }
    public void showResult() {
        toastQuizResult.setText("Тест пройден. " + "\n" +
                "Всего вопросов: " + quiz.getQuestionsNumber() + "\n" +
                "Правильных: " + quiz.getRightAnsweredQuestionNumber());
        toastQuizResult.show();
    }
}
