package ru.geekbrains.android.level1.quiz.models;

import android.content.res.AssetManager;

import java.util.List;

import ru.geekbrains.android.level1.quiz.helpers.QuizData;

/**
 * Created by Валера on 21.08.2017.
 */

public class Quiz {
    private List<Question> quizList;
    private int currentQuestionNumber;
    private int questionsNumber;
    private int rightAnsweredQuestionNumber;

    public Quiz(AssetManager assets) {
        quizList = QuizData.getList(assets);
        currentQuestionNumber = 0;
        questionsNumber = quizList.size();
        rightAnsweredQuestionNumber = 0;
    }

    public Question getNextQuestion() {
        if (currentQuestionNumber == questionsNumber - 1) {
            currentQuestionNumber = 0;
            return getCurrentQuestion();
        } else return quizList.get(currentQuestionNumber++);
    }
    public Question getPreviousQuestion() {
        if (currentQuestionNumber == 0) {
            currentQuestionNumber = quizList.size() - 1;
            return getCurrentQuestion();
        }
        else return quizList.get(--currentQuestionNumber);
    }
    public Question getCurrentQuestion() {
        return quizList.get(currentQuestionNumber);
    }
    public int getRightAnsweredQuestionNumber() {
        return rightAnsweredQuestionNumber;
    }
    public int getQuestionsNumber() {
        return questionsNumber;
    }
    public boolean isPassed() {
        boolean isPassed = true;
        for (int i = 0; i < quizList.size() && isPassed; i++) {
            if (!quizList.get(i).isAnswered()) isPassed = false;
        }
        return isPassed;
    }
    public void incRightAnswers() {
        if (rightAnsweredQuestionNumber < questionsNumber) rightAnsweredQuestionNumber++;
    }
    public void decRightAnswers() {
        if (rightAnsweredQuestionNumber > 0) rightAnsweredQuestionNumber--;
    }
}
