package ru.geekbrains.android.level1.quiz.helpers;

import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.geekbrains.android.level1.quiz.models.Question;

/**
 * Created by Валера on 21.08.2017.
 */

public class QuizData {
    public static final String QUESTIONS_FILE_NAME = "questions";
    public static final String KEYS_FILE_NAME = "keys";
    public static final String LINES_SEPARATOR = "\r\n";
    public static final String ANSWER_TRUE = "да";
    public static final String ANSWER_FALSE = "нет";

    private static String questionsText;
    private static String keysText;

    public static List<Question> getList(AssetManager assets) {
        readFiles(assets);
        return createList();
    }

    private static void readFiles(AssetManager assets) {
        try {
            InputStream inputStreamQuestions = assets.open(QUESTIONS_FILE_NAME);
            InputStream inputStreamKeys = assets.open(KEYS_FILE_NAME);

            byte[] buffer = new byte[inputStreamQuestions.available()];
            inputStreamQuestions.read(buffer);
            questionsText = new String(buffer);

            buffer = new byte[inputStreamKeys.available()];
            inputStreamKeys.read(buffer);
            keysText = new String(buffer);

            inputStreamQuestions.close();
            inputStreamKeys.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static List<Question> createList() {
        List<Question> quizList = new ArrayList<>();

        List<String> questionsTextList = Arrays.asList(questionsText.split(LINES_SEPARATOR));
        List<String> keysTextList = Arrays.asList(keysText.split(LINES_SEPARATOR));
        if (questionsTextList.size() == keysTextList.size()) {
            for (int i = 0; i < questionsTextList.size(); i++) {
                switch (keysTextList.get(i)) {
                    case ANSWER_TRUE:
                        quizList.add(new Question(questionsTextList.get(i), true));
                        break;
                    case ANSWER_FALSE:
                        quizList.add(new Question(questionsTextList.get(i), false));
                        break;
                }
            }
        } else try {
            throw new Exception("Wrong filled files!");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return quizList;
    }
}
