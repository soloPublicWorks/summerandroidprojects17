package ru.geekbrains.android.level1.quiz.controllers;

import android.view.View;
import android.widget.Button;

import ru.geekbrains.android.level1.quiz.QuizActivity;
import ru.geekbrains.android.level1.quiz.models.Quiz;

/**
 * Created by Валера on 21.08.2017.
 */

public class OnClickListenerAnswer implements View.OnClickListener {
    private boolean answerType;
    private Button btnAnswer;
    private Quiz quiz;
    private QuizActivity quizActivity;

    public OnClickListenerAnswer(boolean answerType, Button btnAnswer, Quiz quiz, QuizActivity quizActivity) {
        this.answerType = answerType;
        this.btnAnswer = btnAnswer;
        this.quiz = quiz;
        this.quizActivity = quizActivity;
    }

    @Override
    public void onClick(View view) {
        quiz.getCurrentQuestion().setIsAnswered();
        if (quiz.getCurrentQuestion().getAnswer() == answerType) {
            quiz.incRightAnswers();
            quizActivity.setAnswer(true, btnAnswer);
        } else {
            quiz.decRightAnswers();
            quizActivity.setAnswer(false, btnAnswer);
        }
    }
}
