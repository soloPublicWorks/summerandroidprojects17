package ru.geekbrains.android.level1.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;

public class CalculatorActivity extends AppCompatActivity {
    public static final int DIGITS_NUMBER = 10;
    public static final int OPERATIONS_NUMBER = 4;

    private EditText etExpression;
    private Button[] btnDigits = new Button[DIGITS_NUMBER];
    private Button[] btnOperations = new Button[OPERATIONS_NUMBER]; // +, -, *, /
    private Button btnClear, btnCalculate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        
        initViews();
    }

    private void initViews() {
        etExpression = (EditText) findViewById(R.id.et_expression);

        btnDigits[0] = (Button) findViewById(R.id.btn_0);
        btnDigits[1] = (Button) findViewById(R.id.btn_1);
        btnDigits[2] = (Button) findViewById(R.id.btn_2);
        btnDigits[3] = (Button) findViewById(R.id.btn_3);
        btnDigits[4] = (Button) findViewById(R.id.btn_4);
        btnDigits[5] = (Button) findViewById(R.id.btn_5);
        btnDigits[6] = (Button) findViewById(R.id.btn_6);
        btnDigits[7] = (Button) findViewById(R.id.btn_7);
        btnDigits[8] = (Button) findViewById(R.id.btn_8);
        btnDigits[9] = (Button) findViewById(R.id.btn_9);
        for (int i = 0; i < DIGITS_NUMBER; i++)
            btnDigits[i].setOnClickListener(new OnClickDigit((char) i, etExpression));

        btnOperations[0] = (Button) findViewById(R.id.btn_add);
        btnOperations[1] = (Button) findViewById(R.id.btn_sub);
        btnOperations[2] = (Button) findViewById(R.id.btn_mult);
        btnOperations[3] = (Button) findViewById(R.id.btn_div);
        for (int i = 0; i < DIGITS_NUMBER; i++)
            btnDigits[i].setOnClickListener(new OnClickOperation((char) i, etExpression));

        btnClear = (Button) findViewById(R.id.btn_clear);
        btnCalculate = (Button) findViewById(R.id.btn_calculate);
    }
}
