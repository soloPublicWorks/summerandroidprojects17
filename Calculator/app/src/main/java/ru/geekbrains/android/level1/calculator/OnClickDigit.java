package ru.geekbrains.android.level1.calculator;

import android.view.View;
import android.widget.EditText;

/**
 * Created by Валера on 27.08.2017.
 */

public class OnClickDigit implements View.OnClickListener {
    private char digit;
    private EditText etExpression;

    public OnClickDigit(char digit, EditText etExpression) {
        this.digit = digit;
        this.etExpression = etExpression;
    }

    @Override
    public void onClick(View view) {
        etExpression.setText(etExpression.getText().append(digit));
    }
}
